#!/bin/bash

# Create builder using 

read -r -p "Create docker buildx? "

if [[ $REPLY =~ (y|Y) ]];then
  docker buildx create --name mybuilder --driver docker-container --bootstrap
  docker buildx ls
  #use
  docker buildx use mybuilder
fi


read -r -p "Remove docker buildx? "

if [[ $REPLY =~ (y|Y) ]];then
  docker buildx rm mybuilder
fi
